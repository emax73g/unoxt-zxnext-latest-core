`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:16:04 03/03/2024 
// Design Name: 
// Module Name:    cldac 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module cldac(
    input reset,
    input clk, //18.432 MHz
    input [15:0] audio_l,
    input [15:0] audio_r,
    output mclk,
    output reg sclk,
    output reg lrclk,
    output sdin
    );

	assign mclk = clk;
	
	/*reg [7:0] mclk_cnt;

	always @(posedge clk, posedge reset)
	begin
		if (reset)
			mclk_cnt <= 8'd0;
		else if (mclk_cnt == 8'd1)
		begin
			mclk_cnt <= 8'd0;
			mclk <= !mclk; //12.5MHz
		end
		else
			mclk_cnt <= mclk_cnt + 8'd1;
	end*/
	
	
	reg [7:0] sclk_cnt;
	reg [7:0] lrclk_cnt;
	
	always @(posedge mclk, posedge reset)
	begin
		if (reset)
			sclk_cnt <= 8'd0;
		else if (sclk_cnt == 8'd3)
		begin
			sclk_cnt <= 8'd0;
			sclk <= !sclk; // 2.304 MHz
		end
		else
			sclk_cnt <= sclk_cnt + 8'd1;
	end

	always @(negedge sclk, posedge reset)
	begin
		if (reset)
			lrclk_cnt <= 8'd0;
		else if (lrclk_cnt == 8'd23)
		begin
			lrclk_cnt <= 8'd0;
			lrclk <= !lrclk; //48 KHz
		end
		else
			lrclk_cnt <= lrclk_cnt + 8'd1;
	end
	
	reg [15:0] shift_reg;
	reg [15:0] audio_reg;
	
	always @(negedge sclk)
	begin
		if (lrclk_cnt == 8'd0)
			if (!lrclk)
				shift_reg <= audio_l;
				//shift_reg <= { audio_reg};
			else
				shift_reg <= audio_r;
				//shift_reg <= { audio_reg};
		else
			shift_reg <= { shift_reg[14:0], 1'b0 };
	end
	
	assign sdin = shift_reg[15];

	reg [15:0] audio_cnt;
	
	always @(negedge lrclk)
	begin
		if (audio_cnt == 16'd48)
		begin
			audio_cnt <= 16'd0; // 440 Hz
			if (audio_reg == 16'hffff)
				audio_reg <= 16'h0;
			else
				audio_reg <= 16'hffff;
		end
		else
			audio_cnt <= audio_cnt + 16'd1;
	end

endmodule
